0.0.9

* Use aeson-1.0.
* Add nix build.

0.0.8

* Update after GPS solution parse bug in `stratux-types`.

0.0.7

* Update after bug relating to ConstrainedClassMethods in `stratux-types`.

0.0.6

* Update to `stratux-http`.
* Update to `stratux-types`.

0.0.5

* Update to `stratux-http`.
* Update to `stratux-types`.

0.0.4

* Now depends on `stratux-http`.
* Implements `/getStatus`.

0.0.3

* Now depends on `stratux-websockets`.

0.0.2

* Moved project to `stratux-types` which this one will depend on.

0.0.1

* Initial.
