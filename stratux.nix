{ mkDerivation, base, stdenv, stratux-http, stratux-types
, stratux-websockets
}:
mkDerivation {
  pname = "stratux";
  version = "0.0.12";
  src = ./.;
  libraryHaskellDepends = [
    base stratux-http stratux-types stratux-websockets
  ];
  homepage = "https://gitlab.com/stratux/stratux";
  description = "A library for stratux";
  license = stdenv.lib.licenses.bsd3;
}
