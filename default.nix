{ nixpkgs ? import <nixpkgs> {}, compiler ? "default" }:

let

  inherit (nixpkgs) pkgs;

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  sources = {
    papa = pkgs.fetchFromGitHub {
      owner = "qfpl";
      repo = "papa";
      rev = "97ef00aa45c70213a4f0ce348a2208e3f482a7e3";
      sha256 = "0qm0ay49wc0frxs6ipc10xyjj654b0wgk0b1hzm79qdlfp2yq0n5";
    };

    stratux-types = pkgs.fetchFromGitLab {
      owner = "stratux";
      repo = "stratux-types";
      rev = "4c523092ceb0caceb4d45ef98c4a720ed9a1ac6a";
      sha256 = "16cv092fd4sq18gyqfqm5f334hb01knid3wy10xfypd39kix55m5";
    };

    stratux-websockets = pkgs.fetchFromGitLab {
      owner = "stratux";
      repo = "stratux-websockets";
      rev = "f199642e9c35c8bee8d6c45e1d28dc2dcae331d5";
      sha256 = "0glixq6rw5ad4ywp0fq90rf8n1wy71244h33v7wfa3smgwxqyp65";
    };

    stratux-http = pkgs.fetchFromGitLab {
      owner = "stratux";
      repo = "stratux-http";
      rev = "ac5f4a1be43212b7ff5c57944e45c6c163901c8a";
      sha256 = "0smrxvqzvl0grsxp08xwmwahj5h2jv1zj45mafs5lamry8n0z6kd";
    };

  };

  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: import sources.papa self // {
      stratux-types = import sources.stratux-types {};
      stratux-websockets = import sources.stratux-websockets {};
      stratux-http = import sources.stratux-http {};
      parsers = pkgs.haskell.lib.dontCheck super.parsers;        
    };
  };

  stratux = modifiedHaskellPackages.callPackage ./stratux.nix {};

in

  stratux

